import java.util.*;
import java.io.*;

public class KWIC {
    public HashSet<String> ignorewords;
    public TreeMap<String, ArrayList<String>> mapwordtos;

    String highlightWords (String [] words, int position){
        String newline = "";
        for (int j=0; j< words.length; j++){
            if(j>=1){
                newline += " ";
            }
            if (j == position){
                newline += words[j].toUpperCase();
            } else {
                newline += words[j];
            }
        }
        return newline;
    }

    public HashSet<String> getIgnorewords (Scanner s) {
       ignorewords = new HashSet <String>();
        while (s.hasNextLine()) {
            String line = s.nextLine();
            if (line.equals("::")) {
                break;
            } else {
                ignorewords.add(line.toLowerCase());
            }
        }
        return ignorewords;
    }

    public TreeMap<String, ArrayList<String>> parseSentences (Scanner s) {

        mapwordtos = new TreeMap<>();
        String keyword = "";
        while (s.hasNextLine()){
            String line = s.nextLine().toLowerCase();
            String [] words = line.split(" ");
                for (int i=0 ; i< words.length; i++){
                    if ( ignorewords.contains(words[i])) {
                        continue;
                    }
                      keyword= words[i];
                    if(mapwordtos.containsKey(keyword) == false) {
                        mapwordtos.put(keyword, new ArrayList<String>());
                    }
                    String hline = highlightWords(words,i) ;
                    mapwordtos.get(keyword).add(hline);
                    }
                }
        return mapwordtos;
        }

    public String getOutput(){
        String result = "";
        Set<String> keywords = mapwordtos.keySet();
        for (String k:keywords) {
            List<String> sentences = mapwordtos.get(k);
            for (String s: sentences){
                result += s+"\n" ;
            }
        }
        return result.replace("[","").replace("]","");
    }


    public static void main(String args[]) {
        KWIC kwic = new KWIC();
        Scanner s = new Scanner(System.in);
        kwic.getIgnorewords(s);
        kwic.parseSentences(s);
        //kwic.output();
    }

    }



