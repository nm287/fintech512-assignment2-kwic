import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.*;

class KWICTest {

    @Test
     void testIgnoreWords(){
        String testInput = """
                is
                the
                of
                and
                as
                a
                but
                ::
                Descent of Man
                The Ascent of Man
                The Old Man and The Sea
                A Portrait of The Artist As a Young Man
                A Man is a Man but Bubblesort IS A DOG
                """;
        Scanner s = new Scanner(testInput);
        KWIC kwic = new KWIC();
        HashSet<String> myOut = kwic.getIgnorewords(s);
        assertEquals(7, myOut.size());
        System.out.println(myOut);
        assertEquals(true, myOut.contains("the"));
        assertEquals(false, myOut.contains("THE"));
    }

    @Test
    void testKeyWords(){
        String testInput = """
                                is
                                the
                                of
                                and
                                as
                                a
                                but
                                ::
                                Descent of Man
                                The Ascent of Man
                                The Old Man and The Sea
                                A Portrait of The Artist As a Young Man
                                A Man is a Man but Bubblesort IS A DOG
                                """;
        Scanner s = new Scanner(testInput);
        KWIC kwic = new KWIC();
        
        kwic.getIgnorewords(s);
        //kwic.parseSentences(s);
        TreeMap<String, ArrayList<String>> output = kwic.parseSentences(s);
        //System.out.println(output.get(k));
        //assertEquals(15, output.size());
        assertEquals(true, output.containsKey("man"));
        assertEquals(false, output.containsKey("the"));
    }

    @Test
    void returnSentences(){
        String testInput = """
                                is
                                the
                                of
                                and
                                as
                                a
                                but
                                ::
                                Descent of Man
                                The Ascent of Man
                                The Old Man and The Sea
                                A Portrait of The Artist As a Young Man
                                A Man is a Man but Bubblesort IS A DOG
                                """;
        Scanner s = new Scanner(testInput);
        KWIC kwic = new KWIC();
        kwic.getIgnorewords(s);
        kwic.parseSentences(s);
        //kwic.getOutput();

        String finalresult = kwic.getOutput();
        System.out.println(finalresult);
        //assertEquals(7, kwic.ignorewords.size());
        //assertEquals(15, kwic.keywords.size());
        //assertEquals(true, kwic.mapwordtos.containsKey("Man"));
        //assertEquals(false, kwic.keywords.contains("the"));
    }




}
